# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.16...@pubsweet/logger@0.2.17) (2019-03-06)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.15...@pubsweet/logger@0.2.16) (2019-03-05)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.14...@pubsweet/logger@0.2.15) (2019-02-19)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.13...@pubsweet/logger@0.2.14) (2019-02-19)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.12...@pubsweet/logger@0.2.13) (2019-02-01)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.11...@pubsweet/logger@0.2.12) (2019-01-16)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.10...@pubsweet/logger@0.2.11) (2019-01-14)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.9...@pubsweet/logger@0.2.10) (2019-01-13)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.8...@pubsweet/logger@0.2.9) (2019-01-09)

**Note:** Version bump only for package @pubsweet/logger





## [0.2.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.7...@pubsweet/logger@0.2.8) (2018-12-12)

**Note:** Version bump only for package @pubsweet/logger





<a name="0.2.7"></a>
## [0.2.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.6...@pubsweet/logger@0.2.7) (2018-09-25)




**Note:** Version bump only for package @pubsweet/logger

<a name="0.2.6"></a>
## [0.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.5...@pubsweet/logger@0.2.6) (2018-08-20)




**Note:** Version bump only for package @pubsweet/logger

<a name="0.2.5"></a>
## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.4...@pubsweet/logger@0.2.5) (2018-08-17)




**Note:** Version bump only for package @pubsweet/logger

<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.3...@pubsweet/logger@0.2.4) (2018-06-19)


### Bug Fixes

* **logger:** fix typo in README ([6695674](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6695674))




<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.2...@pubsweet/logger@0.2.3) (2018-05-18)


### Bug Fixes

* use one file at monorepo root ([456f49b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/456f49b))




<a name="0.2.2"></a>

## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/logger@0.2.1...@pubsweet/logger@0.2.2) (2018-02-16)

**Note:** Version bump only for package @pubsweet/logger
