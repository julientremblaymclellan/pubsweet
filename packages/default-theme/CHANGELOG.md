# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [4.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.11...@pubsweet/default-theme@4.0.12) (2019-03-06)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.10...@pubsweet/default-theme@4.0.11) (2019-03-05)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.9...@pubsweet/default-theme@4.0.10) (2019-02-19)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.8...@pubsweet/default-theme@4.0.9) (2019-02-19)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.7...@pubsweet/default-theme@4.0.8) (2019-02-01)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.6...@pubsweet/default-theme@4.0.7) (2019-01-16)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.5...@pubsweet/default-theme@4.0.6) (2019-01-14)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.4...@pubsweet/default-theme@4.0.5) (2019-01-13)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.3...@pubsweet/default-theme@4.0.4) (2019-01-09)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.2...@pubsweet/default-theme@4.0.3) (2018-12-12)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.1...@pubsweet/default-theme@4.0.2) (2018-12-04)

**Note:** Version bump only for package @pubsweet/default-theme





## [4.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@4.0.0...@pubsweet/default-theme@4.0.1) (2018-11-30)

**Note:** Version bump only for package @pubsweet/default-theme





# [4.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@3.0.0...@pubsweet/default-theme@4.0.0) (2018-11-29)


### Features

* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="3.0.0"></a>
# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@2.0.0...@pubsweet/default-theme@3.0.0) (2018-07-02)


### Features

* **ui:** introduce more line height variables ([85c24e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85c24e2))


### BREAKING CHANGES

* **ui:** the existing fontLineHeight variable is gone and replaced by multiple new variables




<a name="2.0.0"></a>
# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@1.0.0...@pubsweet/default-theme@2.0.0) (2018-06-28)


### Code Refactoring

* **ui:** replace current gridunit variables with one small value ([cf48f29](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cf48f29))


### Features

* **ui:** reintroduce warning color ([27943ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/27943ad))


### BREAKING CHANGES

* **ui:** Your ui components will now be multiplying a much smaller value so they need to be
adjusted




<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.2.2...@pubsweet/default-theme@1.0.0) (2018-05-03)


### Bug Fixes

* **theme:** disable unused theme variable ([91691ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/91691ff))
* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))
* **theme:** simplify transitions ([90c72ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90c72ff))


### BREAKING CHANGES

* **theme:** might break components that used the boxShadow variable
* **theme:** transitions might not work for components that used the Xs, S and M values
* **theme:** might break components that used the warning colors




<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.2.1...@pubsweet/default-theme@0.2.2) (2018-04-11)




**Note:** Version bump only for package @pubsweet/default-theme

<a name="0.2.1"></a>
## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.2.0...@pubsweet/default-theme@0.2.1) (2018-03-15)




**Note:** Version bump only for package @pubsweet/default-theme

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/default-theme@0.1.0...@pubsweet/default-theme@0.2.0) (2018-03-05)


### Features

* **default-theme:** add variables to default theme ([ba121b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba121b0))
* **normalize:** add normalize css ([9eb24e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9eb24e5))
* **ui:** add theming to Tags ([ee959d2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee959d2))
* **ui:** add theming to ValidatedField ([c2a1d54](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2a1d54))




<a name="0.1.0"></a>

# 0.1.0 (2018-02-08)

### Features

* **theme:** add default theme package ([5231a56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5231a56))
