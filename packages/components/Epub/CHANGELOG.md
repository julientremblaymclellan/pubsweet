# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.5.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.14...pubsweet-component-epub@0.5.15) (2019-03-06)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.13...pubsweet-component-epub@0.5.14) (2019-03-05)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.12...pubsweet-component-epub@0.5.13) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.11...pubsweet-component-epub@0.5.12) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.10...pubsweet-component-epub@0.5.11) (2019-02-01)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.9...pubsweet-component-epub@0.5.10) (2019-01-16)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.8...pubsweet-component-epub@0.5.9) (2019-01-14)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.7...pubsweet-component-epub@0.5.8) (2019-01-13)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.6...pubsweet-component-epub@0.5.7) (2019-01-09)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.5...pubsweet-component-epub@0.5.6) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.4...pubsweet-component-epub@0.5.5) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.3...pubsweet-component-epub@0.5.4) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-epub





## [0.5.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.2...pubsweet-component-epub@0.5.3) (2018-11-29)

**Note:** Version bump only for package pubsweet-component-epub





<a name="0.5.2"></a>
## [0.5.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.1...pubsweet-component-epub@0.5.2) (2018-11-16)


### Bug Fixes

* highlight js dependency update ([0fb0733](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0fb0733))




<a name="0.5.1"></a>
## [0.5.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.5.0...pubsweet-component-epub@0.5.1) (2018-11-13)


### Bug Fixes

* update html-epub dependency ([5f55907](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5f55907))




<a name="0.5.0"></a>
# [0.5.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.4.0...pubsweet-component-epub@0.5.0) (2018-10-12)


### Features

* conversion of inline notes ([deb2930](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/deb2930))
* download html for pagedJS ([cec190e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cec190e))
* paged exporter ([250510b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/250510b))




<a name="0.4.0"></a>
# [0.4.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.3.5...pubsweet-component-epub@0.4.0) (2018-10-08)


### Features

* **$epub:** additional converter for ucp ([293274d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/293274d))
* **epub:** additional converter for UCP ([87b3041](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/87b3041))




<a name="0.3.5"></a>
## [0.3.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.3.4...pubsweet-component-epub@0.3.5) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.3.4"></a>
## [0.3.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.3.3...pubsweet-component-epub@0.3.4) (2018-09-19)


### Bug Fixes

* exported notes and fix notes on different lines ([1336da6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1336da6))




<a name="0.3.3"></a>
## [0.3.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.3.2...pubsweet-component-epub@0.3.3) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.3.2"></a>
## [0.3.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.3.1...pubsweet-component-epub@0.3.2) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.3.1"></a>
## [0.3.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.3.0...pubsweet-component-epub@0.3.1) (2018-07-12)


### Bug Fixes

* **epub server:** bugfix on a dependency ([a78d1b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a78d1b3))




<a name="0.3.0"></a>
# [0.3.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.16...pubsweet-component-epub@0.3.0) (2018-06-19)


### Bug Fixes

* **epub:** add code snippets to epub export ([375d5dc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/375d5dc))
* linting errors ([03c52da](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/03c52da))


### Features

* add figure and figcaption on export ([6b3ebd6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6b3ebd6))
* add highlight js to export ([ef08e72](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ef08e72))
* styles for the figcaption ([ae19a09](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ae19a09))




<a name="0.2.16"></a>
## [0.2.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.15...pubsweet-component-epub@0.2.16) (2018-06-01)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.15"></a>
## [0.2.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.14...pubsweet-component-epub@0.2.15) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.14"></a>
## [0.2.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.13...pubsweet-component-epub@0.2.14) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.13"></a>
## [0.2.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.12...pubsweet-component-epub@0.2.13) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-epub

<a name="0.2.12"></a>

## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.11...pubsweet-component-epub@0.2.12) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-epub
