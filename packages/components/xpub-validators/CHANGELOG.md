# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.9...xpub-validators@0.0.10) (2019-03-06)

**Note:** Version bump only for package xpub-validators





## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.8...xpub-validators@0.0.9) (2019-03-05)

**Note:** Version bump only for package xpub-validators





## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.7...xpub-validators@0.0.8) (2019-02-19)

**Note:** Version bump only for package xpub-validators





## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.6...xpub-validators@0.0.7) (2019-02-19)

**Note:** Version bump only for package xpub-validators





<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.5...xpub-validators@0.0.6) (2018-09-04)


### Bug Fixes

* **fomrbuilder:** fix validation ([98b3b5e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98b3b5e))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.4...xpub-validators@0.0.5) (2018-04-03)




**Note:** Version bump only for package xpub-validators

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/xpub-validators@0.0.3...xpub-validators@0.0.4) (2018-03-15)




**Note:** Version bump only for package xpub-validators

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package xpub-validators
