## pubsweet-component-xpub-manuscript

A PubSweet component that provides the interface for an author to edit their manuscript.
