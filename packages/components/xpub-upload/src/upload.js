import uploadFile from './no-redux'

export default file => dispatch => uploadFile(file)
