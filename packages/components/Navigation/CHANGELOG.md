# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.12...pubsweet-component-navigation@1.0.13) (2019-03-06)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.11...pubsweet-component-navigation@1.0.12) (2019-03-05)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.10...pubsweet-component-navigation@1.0.11) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.9...pubsweet-component-navigation@1.0.10) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.8...pubsweet-component-navigation@1.0.9) (2019-02-01)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.7...pubsweet-component-navigation@1.0.8) (2019-01-16)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.6...pubsweet-component-navigation@1.0.7) (2019-01-14)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.5...pubsweet-component-navigation@1.0.6) (2019-01-13)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.4...pubsweet-component-navigation@1.0.5) (2019-01-09)

**Note:** Version bump only for package pubsweet-component-navigation





## [1.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.3...pubsweet-component-navigation@1.0.4) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-navigation





<a name="1.0.3"></a>
## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.2...pubsweet-component-navigation@1.0.3) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-navigation

<a name="1.0.2"></a>
## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.1...pubsweet-component-navigation@1.0.2) (2018-03-27)


### Bug Fixes

* resolve remaining jsx-a11y lint issues ([a75c0de](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a75c0de))




<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@1.0.0...pubsweet-component-navigation@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-navigation

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-navigation@0.3.2...pubsweet-component-navigation@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
