# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/job-xsweet@1.1.1...@pubsweet/job-xsweet@1.1.2) (2019-03-06)

**Note:** Version bump only for package @pubsweet/job-xsweet





## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/job-xsweet@1.1.0...@pubsweet/job-xsweet@1.1.1) (2019-03-05)


### Bug Fixes

* **job-xsweet:** improve/fix parallel processing of documents ([3233883](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3233883))
* **job-xsweet:** use exact Saxon 9-8-0-1J ([2680539](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2680539))
* **job-xsweet:** use Saxon 9.8 ([17fcf17](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/17fcf17))





# 1.1.0 (2019-02-19)


### Features

* add job-xsweet component ([f3441b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f3441b0))
