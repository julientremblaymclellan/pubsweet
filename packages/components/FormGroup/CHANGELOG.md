# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.7...pubsweet-component-form-group@2.0.8) (2019-03-06)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.6...pubsweet-component-form-group@2.0.7) (2019-03-05)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.5...pubsweet-component-form-group@2.0.6) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.4...pubsweet-component-form-group@2.0.5) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.3...pubsweet-component-form-group@2.0.4) (2019-02-01)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.2...pubsweet-component-form-group@2.0.3) (2019-01-16)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.1...pubsweet-component-form-group@2.0.2) (2019-01-14)

**Note:** Version bump only for package pubsweet-component-form-group





## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@2.0.0...pubsweet-component-form-group@2.0.1) (2019-01-13)

**Note:** Version bump only for package pubsweet-component-form-group





# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.33...pubsweet-component-form-group@2.0.0) (2019-01-09)


### Features

* **components:** remove FormGroup component ([507b242](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/507b242))


### BREAKING CHANGES

* **components:** FormGroup component has been removed as it is unused. It also uses the old
model/validation system, that will shortly no longer exist.





## [1.1.33](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.32...pubsweet-component-form-group@1.1.33) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-form-group





## [1.1.32](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.31...pubsweet-component-form-group@1.1.32) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-form-group





## [1.1.31](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.30...pubsweet-component-form-group@1.1.31) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-form-group





## [1.1.30](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.29...pubsweet-component-form-group@1.1.30) (2018-11-29)

**Note:** Version bump only for package pubsweet-component-form-group





<a name="1.1.29"></a>
## [1.1.29](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.28...pubsweet-component-form-group@1.1.29) (2018-11-13)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.28"></a>
## [1.1.28](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.27...pubsweet-component-form-group@1.1.28) (2018-11-05)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.27"></a>
## [1.1.27](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.26...pubsweet-component-form-group@1.1.27) (2018-10-17)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.26"></a>
## [1.1.26](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.25...pubsweet-component-form-group@1.1.26) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.25"></a>
## [1.1.25](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.24...pubsweet-component-form-group@1.1.25) (2018-09-29)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.24"></a>
## [1.1.24](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.23...pubsweet-component-form-group@1.1.24) (2018-09-28)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.23"></a>
## [1.1.23](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.22...pubsweet-component-form-group@1.1.23) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.22"></a>
## [1.1.22](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.21...pubsweet-component-form-group@1.1.22) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.21"></a>
## [1.1.21](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.20...pubsweet-component-form-group@1.1.21) (2018-09-25)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.20"></a>
## [1.1.20](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.19...pubsweet-component-form-group@1.1.20) (2018-09-20)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.19"></a>
## [1.1.19](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.18...pubsweet-component-form-group@1.1.19) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.18"></a>
## [1.1.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.17...pubsweet-component-form-group@1.1.18) (2018-09-04)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.17"></a>
## [1.1.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.16...pubsweet-component-form-group@1.1.17) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.16"></a>
## [1.1.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.15...pubsweet-component-form-group@1.1.16) (2018-08-17)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.15"></a>
## [1.1.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.14...pubsweet-component-form-group@1.1.15) (2018-07-09)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.14"></a>
## [1.1.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.13...pubsweet-component-form-group@1.1.14) (2018-06-19)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.13"></a>
## [1.1.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.12...pubsweet-component-form-group@1.1.13) (2018-05-18)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.12"></a>
## [1.1.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.11...pubsweet-component-form-group@1.1.12) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.11"></a>
## [1.1.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.10...pubsweet-component-form-group@1.1.11) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.10"></a>
## [1.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.9...pubsweet-component-form-group@1.1.10) (2018-04-25)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.9"></a>
## [1.1.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.8...pubsweet-component-form-group@1.1.9) (2018-04-24)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.8"></a>
## [1.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.7...pubsweet-component-form-group@1.1.8) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.7"></a>
## [1.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.6...pubsweet-component-form-group@1.1.7) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.6"></a>
## [1.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.5...pubsweet-component-form-group@1.1.6) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.5"></a>
## [1.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.4...pubsweet-component-form-group@1.1.5) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.4"></a>
## [1.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.3...pubsweet-component-form-group@1.1.4) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.3"></a>
## [1.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.2...pubsweet-component-form-group@1.1.3) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.2"></a>
## [1.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.1...pubsweet-component-form-group@1.1.2) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.1"></a>

## [1.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.1.0...pubsweet-component-form-group@1.1.1) (2018-03-09)

**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.1.0"></a>

# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.0.2...pubsweet-component-form-group@1.1.0) (2018-03-05)

### Bug Fixes

* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))
* **components:** styleguide can render components using validations ([93df7af](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/93df7af))
* restore FormGroup to its previous state, for later deletion ([3135ffd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3135ffd))

### Features

* **elife-theme:** add elife theme ([e406e0d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e406e0d))

<a name="1.0.2"></a>

## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.0.1...pubsweet-component-form-group@1.0.2) (2018-02-23)

**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@1.0.0...pubsweet-component-form-group@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-form-group

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@0.1.10...pubsweet-component-form-group@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16

<a name="0.1.10"></a>

## [0.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-form-group@0.1.9...pubsweet-component-form-group@0.1.10) (2018-02-02)

**Note:** Version bump only for package pubsweet-component-form-group
