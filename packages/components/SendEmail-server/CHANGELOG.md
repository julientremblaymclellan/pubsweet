# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.12...@pubsweet/component-send-email@0.2.13) (2019-03-06)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.11...@pubsweet/component-send-email@0.2.12) (2019-03-05)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.10...@pubsweet/component-send-email@0.2.11) (2019-02-19)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.9...@pubsweet/component-send-email@0.2.10) (2019-02-19)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.8...@pubsweet/component-send-email@0.2.9) (2019-02-01)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.7...@pubsweet/component-send-email@0.2.8) (2019-01-16)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.6...@pubsweet/component-send-email@0.2.7) (2019-01-14)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.5...@pubsweet/component-send-email@0.2.6) (2019-01-13)

**Note:** Version bump only for package @pubsweet/component-send-email





## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.4...@pubsweet/component-send-email@0.2.5) (2019-01-09)

**Note:** Version bump only for package @pubsweet/component-send-email





<a name="0.2.4"></a>
## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.3...@pubsweet/component-send-email@0.2.4) (2018-06-19)


### Bug Fixes

* **send-email-server:** use config package according to documentation ([d05d72f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d05d72f))




<a name="0.2.3"></a>
## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.2...@pubsweet/component-send-email@0.2.3) (2018-04-03)




**Note:** Version bump only for package @pubsweet/component-send-email

<a name="0.2.2"></a>
## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-send-email@0.2.1...@pubsweet/component-send-email@0.2.2) (2018-03-27)




**Note:** Version bump only for package @pubsweet/component-send-email

<a name="0.2.1"></a>
## 0.2.1 (2018-03-19)


### Bug Fixes

* **send-email:** rename for styleguide ([0a5b0a5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0a5b0a5))
