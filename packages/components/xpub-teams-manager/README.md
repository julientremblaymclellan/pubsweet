## pubsweet-component-xpub-teams-manager

A PubSweet component that provides interface for admin to manage the teams of Xpub
by assigning Managing Editor, Handling Editor, Senior Editor.
