# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [6.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@6.0.3...pubsweet-component-xpub-submit@6.0.4) (2019-03-06)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [6.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@6.0.2...pubsweet-component-xpub-submit@6.0.3) (2019-03-05)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [6.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@6.0.1...pubsweet-component-xpub-submit@6.0.2) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [6.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@6.0.0...pubsweet-component-xpub-submit@6.0.1) (2019-02-19)

**Note:** Version bump only for package pubsweet-component-xpub-submit





# [6.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.7...pubsweet-component-xpub-submit@6.0.0) (2019-02-01)


### Bug Fixes

* **eslint:** submit form ([1462c10](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1462c10))
* **styleguide:** temporarily disable styleguide ([e519ed1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e519ed1))
* **submit:** fixing submit confirm button ([8aee1e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8aee1e5))


### Code Refactoring

* temporarily remove unmigrated components ([32db6ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32db6ad))


### BREAKING CHANGES

* A lot of unmigrated (not yet moved from REST/Redux to GraphQL/Apollo system) bits
have changed. There might be some breaking changes as a result. This is a big migration involving
big changes - if you encounter anything weird, please contact us on GitLab or on Mattermost.





## [5.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.6...pubsweet-component-xpub-submit@5.0.7) (2019-01-16)


### Bug Fixes

* **components:** fixing components after new manuscript version ([89537ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/89537ff))
* **components:** graphql data model changes ([4b61093](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4b61093))
* **graphql:** review components fixes ([8094d9e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8094d9e))
* **merge:** merging to master ([8603808](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8603808))
* **submit:** intro template ([6c850e1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6c850e1))
* **submit:** intro template fix eslint ([fcc2599](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fcc2599))
* **test:** formbuilder ([93c55fd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/93c55fd))
* **test:** problems with eslint and test ([48f7fe2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48f7fe2))
* **xpub-review:** changes tp reviews ([5ae4240](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5ae4240))





## [5.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.5...pubsweet-component-xpub-submit@5.0.6) (2019-01-14)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [5.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.4...pubsweet-component-xpub-submit@5.0.5) (2019-01-13)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [5.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.3...pubsweet-component-xpub-submit@5.0.4) (2019-01-09)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [5.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.2...pubsweet-component-xpub-submit@5.0.3) (2018-12-12)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [5.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.1...pubsweet-component-xpub-submit@5.0.2) (2018-12-04)

**Note:** Version bump only for package pubsweet-component-xpub-submit





## [5.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@5.0.0...pubsweet-component-xpub-submit@5.0.1) (2018-11-30)

**Note:** Version bump only for package pubsweet-component-xpub-submit





# [5.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.2.0...pubsweet-component-xpub-submit@5.0.0) (2018-11-29)


### Features

* **various:** update styled-components ([5c51466](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5c51466))
* **various:** upgrade styled-components ([9b886f6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9b886f6))


### BREAKING CHANGES

* **various:** Replace all styled-components .extend with styled()
* **various:** Replace styled-components injectGlobal with new createGlobalStyle





<a name="4.2.0"></a>
# [4.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.1.4...pubsweet-component-xpub-submit@4.2.0) (2018-11-05)


### Features

* GraphQL Xpub submit component ([ba07060](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba07060))




<a name="4.1.4"></a>
## [4.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.1.3...pubsweet-component-xpub-submit@4.1.4) (2018-10-08)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.1.3"></a>
## [4.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.1.2...pubsweet-component-xpub-submit@4.1.3) (2018-09-27)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.1.2"></a>
## [4.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.1.1...pubsweet-component-xpub-submit@4.1.2) (2018-09-19)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.1.1"></a>
## [4.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.1.0...pubsweet-component-xpub-submit@4.1.1) (2018-09-06)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.1.0"></a>
# [4.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.10...pubsweet-component-xpub-submit@4.1.0) (2018-09-04)


### Bug Fixes

* **editor:** remove warnings ([b563efd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b563efd))
* **fomrbuilder:** fix validation ([98b3b5e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/98b3b5e))
* **foormbuilder:** linter prettier ([db290ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/db290ff))
* **formbuilder:** create folder from scratch ([a2d533e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/a2d533e))
* **formbuilder:** fixing styling issues to submit ([54bc9a3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54bc9a3))
* **submit:** fixing authors save field ([bb89f08](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bb89f08))
* **submit:** form was updated correctly ([94f9844](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/94f9844))


### Features

* **formbuilder:** add formbuilder component ([c24b9f7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c24b9f7))
* **formbuilder:** add validation for elements ([882935a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/882935a))
* **submit:** import dynamically the form template ([ac4649e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ac4649e))




<a name="4.0.10"></a>
## [4.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.9...pubsweet-component-xpub-submit@4.0.10) (2018-08-22)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.9"></a>
## [4.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.8...pubsweet-component-xpub-submit@4.0.9) (2018-08-20)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.8"></a>
## [4.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.7...pubsweet-component-xpub-submit@4.0.8) (2018-08-17)


### Bug Fixes

* **actions:** validationStatus fix ([762432f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/762432f))




<a name="4.0.7"></a>
## [4.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.6...pubsweet-component-xpub-submit@4.0.7) (2018-08-02)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.6"></a>
## [4.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.5...pubsweet-component-xpub-submit@4.0.6) (2018-07-27)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.5"></a>
## [4.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.4...pubsweet-component-xpub-submit@4.0.5) (2018-07-23)


### Bug Fixes

* **submit:** authors Input start open ([03bbe31](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/03bbe31))
* **submit:** fix lint error ([d6c2077](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d6c2077))




<a name="4.0.4"></a>
## [4.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.3...pubsweet-component-xpub-submit@4.0.4) (2018-07-19)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.3"></a>
## [4.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.2...pubsweet-component-xpub-submit@4.0.3) (2018-07-12)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.2"></a>
## [4.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.1...pubsweet-component-xpub-submit@4.0.2) (2018-07-09)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.1"></a>
## [4.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@4.0.0...pubsweet-component-xpub-submit@4.0.1) (2018-07-03)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="4.0.0"></a>
# [4.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@3.0.1...pubsweet-component-xpub-submit@4.0.0) (2018-07-02)


### Features

* **ui:** introduce more line height variables ([85c24e2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/85c24e2))


### BREAKING CHANGES

* **ui:** the existing fontLineHeight variable is gone and replaced by multiple new variables




<a name="3.0.1"></a>
## [3.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@3.0.0...pubsweet-component-xpub-submit@3.0.1) (2018-06-28)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="3.0.0"></a>
# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@2.0.1...pubsweet-component-xpub-submit@3.0.0) (2018-06-28)


### Bug Fixes

* **submit:** add text SubNotes to submit ([d034b05](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d034b05))
* **submit:** change texts get from config ([fbd209f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fbd209f))


### Code Refactoring

* **ui:** replace current gridunit variables with one small value ([cf48f29](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cf48f29))


### BREAKING CHANGES

* **ui:** Your ui components will now be multiplying a much smaller value so they need to be
adjusted




<a name="2.0.1"></a>
## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@2.0.0...pubsweet-component-xpub-submit@2.0.1) (2018-06-19)


### Bug Fixes

* **pubsweet-ui:** tests are failing ([0e57798](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e57798))




<a name="2.0.0"></a>
# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.2.2...pubsweet-component-xpub-submit@2.0.0) (2018-06-01)


### Bug Fixes

* **components:** submit button show ([2635368](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2635368))


### Features

* **ui:** start ui-toolkit module ([2083b9c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2083b9c))


### BREAKING CHANGES

* **ui:** th now comes from the toolkit, so all th imports from ui are now broken




<a name="1.2.2"></a>
## [1.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.2.1...pubsweet-component-xpub-submit@1.2.2) (2018-05-21)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="1.2.1"></a>
## [1.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.2.0...pubsweet-component-xpub-submit@1.2.1) (2018-05-18)


### Bug Fixes

* **components:** add tests to suggestions component ([50777b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/50777b3))
* **components:** rewrite conditional checks to more clean ([c41d79d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c41d79d))
* **components:** submit submitted versions ([48d07ee](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/48d07ee))




<a name="1.2.0"></a>
# [1.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.1.0...pubsweet-component-xpub-submit@1.2.0) (2018-05-10)


### Bug Fixes

* **components:** fix lint errors ([4e22ec1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e22ec1))
* **components:** fix linting issues ([4385b58](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4385b58))
* **components:** fixes in linter ([7c31f6b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c31f6b))
* **components:** html parse, styled components ([8b24552](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8b24552))
* **components:** linter ([9aac3fa](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9aac3fa))
* **components:** merge two commponets two one ([4e2ed76](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e2ed76))
* **components:** redirect submission add selectors ([53db5a7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/53db5a7))
* **components:** review page layout ([4ea2cdd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4ea2cdd))
* **components:** take care of case of zero files ([82cff08](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/82cff08))
* **components:** title wording ([0c293f4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0c293f4))


### Features

* **components:** add columns to submission and tabs ([40470a0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/40470a0))
* **components:** add current version files ([4c77f3c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4c77f3c))
* **components:** add tabs to submission ([0e45892](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0e45892))
* **components:** create accordion component ([05a23e4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/05a23e4))
* **components:** create accordion component ([54f5b7d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/54f5b7d))




<a name="1.1.0"></a>
# [1.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@1.0.0...pubsweet-component-xpub-submit@1.1.0) (2018-05-09)


### Bug Fixes

* fixed misnamed redux form props in authors input ([940edc0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/940edc0))
* fixed misnamed redux form props in authors input ([bb4af56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bb4af56))
* fixed misnamed redux form props in authors input ([fb362b2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fb362b2))


### Features

* add AuthorsInput component ([f7d12b3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f7d12b3))
* authors input, added padding around fields ([1e5d742](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1e5d742))
* authors input, fixed merge error ([c908fa4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c908fa4))
* authors input, fixed prettier errors ([0657143](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0657143))
* authors input,component  updated to ensure at least one author ([d43dd92](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d43dd92))
* list styles for authors input ([3f85bbd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3f85bbd))
* two inputs per line ([aa0544a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa0544a))
* update MetadataFields to use AuthorsInput component ([fa1640e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/fa1640e))
* update MetadataFields to use AuthorsInput component ([1baac87](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/1baac87))
* update MetadataFields to use AuthorsInput component ([355f282](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/355f282))




<a name="1.0.0"></a>
# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.1.1...pubsweet-component-xpub-submit@1.0.0) (2018-05-03)


### Bug Fixes

* **theme:** remove warning color ([c0897c8](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c0897c8))


### BREAKING CHANGES

* **theme:** might break components that used the warning colors




<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.1.0...pubsweet-component-xpub-submit@0.1.1) (2018-05-03)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.10...pubsweet-component-xpub-submit@0.1.0) (2018-04-24)


### Bug Fixes

* **compoenents:** fix cases of empty objects in metadata ([7a5bfbc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7a5bfbc))
* **component:** put striphtml function back to place ([2a69dca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2a69dca))
* **components:** change value to files at upload components ([aa2b45e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/aa2b45e))
* **xpub-submit:** use no-redux version of uploadFile ([cc904a3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cc904a3))


### Features

* **xpub-submit:** move GraphQL functionality into separate component ([cfb2a81](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cfb2a81))




<a name="0.0.10"></a>
## [0.0.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.9...pubsweet-component-xpub-submit@0.0.10) (2018-04-11)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.9"></a>
## [0.0.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.8...pubsweet-component-xpub-submit@0.0.9) (2018-04-03)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.8"></a>
## [0.0.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.7...pubsweet-component-xpub-submit@0.0.8) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.7"></a>
## [0.0.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.6...pubsweet-component-xpub-submit@0.0.7) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.6"></a>
## [0.0.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.5...pubsweet-component-xpub-submit@0.0.6) (2018-03-27)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.4...pubsweet-component-xpub-submit@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-submit@0.0.3...pubsweet-component-xpub-submit@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-submit

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package pubsweet-component-xpub-submit
