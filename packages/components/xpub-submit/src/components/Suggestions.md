A list of questions that must be answered before submission.

```js
const manuscript = {
  suggestions: {
    reviewers: {
      opposed: faker.name.findName(),
    },
  },
}
;<Suggestions manuscript={manuscript} />
```
