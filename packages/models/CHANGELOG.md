# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.3...@pubsweet/models@0.2.4) (2019-03-06)

**Note:** Version bump only for package @pubsweet/models





## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.2...@pubsweet/models@0.2.3) (2019-03-05)

**Note:** Version bump only for package @pubsweet/models





## [0.2.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.1...@pubsweet/models@0.2.2) (2019-02-19)

**Note:** Version bump only for package @pubsweet/models





## [0.2.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.2.0...@pubsweet/models@0.2.1) (2019-02-19)

**Note:** Version bump only for package @pubsweet/models





# [0.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.3...@pubsweet/models@0.2.0) (2019-02-01)


### Features

* **models:** support for multiple models in a single component ([caed5be](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/caed5be))





## [0.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.2...@pubsweet/models@0.1.3) (2019-01-16)

**Note:** Version bump only for package @pubsweet/models





## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.1...@pubsweet/models@0.1.2) (2019-01-14)

**Note:** Version bump only for package @pubsweet/models





## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/models@0.1.0...@pubsweet/models@0.1.1) (2019-01-13)

**Note:** Version bump only for package @pubsweet/models





# 0.1.0 (2019-01-09)


### Features

* introduce [@pubsweet](https://gitlab.coko.foundation/pubsweet)/models package ([7c1a364](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7c1a364))
